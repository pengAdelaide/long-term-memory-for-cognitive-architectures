# Long-Term Memory for Cognitive Architectures


## Author(s)
*   peng wang (University of Adelaide)

--------------------------------------------------------------------------------

## Table of Content

1. Introduction and motivation

2. Background

3. Activation circuits

4. Memristive CAMs

5. A case study

6. CAMs with spreading Activation

7. Conclusion and future work